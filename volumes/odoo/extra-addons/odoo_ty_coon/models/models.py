# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import Warning
from .constants import *


class OdooTyCoonProductTemplate(models.Model):
    _name = 'product.template'
    _inherit = 'product.template'

    unlockCost = fields.Float('Unlock cost', default=DEFAULT_CASH)
    unlocked = fields.Boolean('Unlocked', default=False)

    def unlock_product(self):
        game_manager = self.env['odoo_tycoon.game_manager'].search([('name', '=', default_name)])

        if game_manager.cash >= self.unlockCost:
            self.unlocked = True
            game_manager.cash -= self.unlockCost
        else:
            raise Warning(f'Not enough money to unlock this {self.name} !')


class OdooTycoonUnlockCosts(models.Model):
    _name = "odoo_tycoon.unlockcosts"

    type = fields.Char("Cost type")
    value = fields.Float("Cost values")


class OdooTyCoonGameManager(models.Model):
    _name = 'odoo_tycoon.game_manager'

    name = fields.Char("Game name", default=DEFAULT_NAME)
    day = fields.Integer("Current day", default=DEFAULT_DAY)
    cash = fields.Float("Cash", default=DEFAULT_CASH)
    award = fields.Float("Award", default=DEFAULT_AWARD)
    # unlock_costs = fields.One2many(comodel_name="odoo_tycoon.unlockcosts")

    @property
    def is_admin(self):
        pass

    @property
    def can_unlock(self):
        pass

    def go_next_date(self, dates_count=1):
        self.write({
            'day': self.day + dates_count,
            # 'cash': -self.cash * dates_count,
            'award': self.award + self.get_awards(dates_count)
        })

    def get_awards(self, dates_count=1):
        # getting unlocked products of this game
        products = self.env['product.template'].search([(
            'unlocked', '=', True
        )])

        total_awards_per_day = 0

        # loop through each product that this game have unlocked and summing to the total awards
        for product in products:
            total_awards_per_day += product.list_price * 10

        # returning the total awards based-on the dates count
        return total_awards_per_day * dates_count

    def skip_five_dates(self):
        self.go_next_date(dates_count=5)

    def skip_thirty_dates(self):
        self.go_next_date(dates_count=30)

    def reset(self):
        products = self.env['product.template'].search([(
            'unlocked', '=', True
        )]).write({'unlocked': False})

        self.write({
            'day': DEFAULT_DAY,
            'cash': DEFAULT_CASH,
            'award': DEFAULT_AWARD
        })

    def unlock(self):
        raise Warning('Developing')
